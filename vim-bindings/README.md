# Vim Bindings for Editing PreTeXt files

In many ways, Vim is not the best editor for editing XML files. 
But Vim is also the one I am most used to. 
Here are some key bindings to make Vim more useful for editing PreTeXt files. 

## Installation

### Nvim

Copy the file `pretext_kw.txt` to `~/.config/nvim/custom/` (create directory if necessary; you can put into another directory if you wish, but you will have to edit the `xml.vim` file to adjust the path. 

Copy the file `xml.vim` to `~/.config/nvim/ftplugin/` (create directory if necessary). 

### Vim

Same as Nvim, but put the files into appropriate subdirectories in `~/.vim/`

The line `setlocal dict+=~/.config/nvim/custom/pretext_kw.txt` in `xml.vim` should be edited to point to the correct path. 

## Use

The `pretext_kw.txt` collects the XML tags defined by PreTeXt. 
You can use it through keyword completion `<C-x><C-k>`

### Insert Mode Mappings

- Autocompletion of PreTeXt XML-tags. The key `<` is "active": its behavior depends on the subsequent keystroke. 
   - If it is a character in `isk` then it is interpreted as trying to type an XML tag. Dictionary completion is used to suggest valid PreTeXt XML-tags.
   - If it is `!` assume we want to write an XML comment. 
   - If it is `/` assume we want to close the XML tag. Use Omni-complete to autoclose. 
   - For anything else assume we want the `<` literal, which in the PreTeXt context requires converting it to `\lt`. 
- Tag closing using `]]`: After typing `<title>This Chapter`, hitting `]]` closes to `<title>This Chapter </title>`
- Common "mathmode" characters: `<<` -> `\lt` ;  `>>` -> `\gt` ;  `&&` -> `\amp`
- Short hands for character entities: `&<` -> `&lt;` and `&>` -> `&gt;`
- For those of you used to LaTeX mathmodes, the following abbreviations are set to enter mathmode. To activate type the shown keys and then hit `<space>`
   - `\(` becomes `<m>`
   - `\[` becomes `<me>`
   - `\[n` becomes `<men>`, with carriage return entered.
   - `\[a` becomes `<md>`, with carriage return, and on the next line pre-filled with `<mrow>`.
   - `#\[a` same as above but with `<mdn>`.
- For tagging equations with symbolic tags, the key combination `##t` brings up a menu of the available symbolic tags. 
- For numbering individual rows within an `<md>`, use `##n` to insert `number="yes"`
- Setting `xml:id` of tags: With cursor after `E` in `<title NAME>`, hitting `###` transforms to `<title xml:id="NAME">`. 
- `<C-p>` and `<C-n>` can be used as usual to complete `xml:id` labels when writing references. 
- For convenience: 
    - `<S-tab>` is mapped to `<C-p>`; this will bring-up autocompletion in insert mode; and when the pop-up menu is active, this allows reverse cycling through the options. 
    - `<tab>` detects whether pop-up menu is open; when it is, it selects the highlighted item; if not, it acts as normal tab. 
- After `NAME`, hitting `@@` transforms to `<xref ref="NAME" />` for cross references. 

### Normal Mode Mappings

- Since in XML the normal section navigations are rather useless, they have been co-opted to find XML tags. For the search, self-closing tags are considered both opening and closing. The cursor is placed at the closing angle bracket `>` of the tag, since during editing it is more likely one wants to add text either after the tag or add properties inside the tag. 
   - `]]` finds next closing tag
   - `][` finds next opening tag
   - `[[` finds previous opening tag
   - `[]` finds previous closing tag. 
- The `(` and `)` navigations are co-opted to search for structural tags for the writing in PreTeXt; these include things like `<chapter>`, `<subsection>`, and `<exercises>`. See the file itself for the full listing. 
