setlocal tabstop=2
setlocal shiftwidth=2
setlocal softtabstop=2
setlocal expandtab
setlocal autoindent

"""""  General XML stuff """""
" Autoclose tag
inoremap ]] </<C-x><C-o>

" Remap the navigation keys.
" The [[ [] ]] ][ commands by default places the cursor at the closing >
" Self closing tags count as both open and close
" ]] - goes to the next closing tag
nmap ]] /<[^>]*\/[^<]*><CR>f>
" ][ - goes to the next opening tag.
nmap ][ /<[^/]<CR>f>
" [] - goes to the previous closing tag.
nmap [] ?><CR>?<[^>]*\/[^<]*><CR>f>
" [[ - goes to the previous opening tag
nmap [[ ?><CR>?<[^/]<CR>f>
" The ( and ) keys are for finding the prev/next divisional tags
nmap ( ?<\(book\\|article\\|part\\|chapter\\|section\\|subsection\\|subsubsection\\|paragraphs\\|introduction\\|conclusion\\|exercises\\|references\)<CR>
nmap ) /<\(book\\|article\\|part\\|chapter\\|section\\|subsection\\|subsubsection\\|paragraphs\\|introduction\\|conclusion\\|exercises\\|references\)<CR>
"""


"""""  PreTeXt specific stuff """""
" Dictionary file for using PreTeXt
setlocal isk+=-
setlocal dict+=~/.config/nvim/custom/pretext_kw.txt
" Set completion to be more like standard IDEs
setlocal cot=noinsert,menuone
" Open angle brace is active. 
inoremap < <C-R>=OpenLT()<CR>
" Use tab to complete selection
inoremap <expr> <tab> pumvisible() ? "\<C-y>" : "\<tab>"
" Map S-tab to reverse search completion; this also allows it to be used to
" start completion
inoremap <S-tab> <C-p>
" Behavior of < depends on the subsequent character. 
func! OpenLT()
	let nextchar = nr2char(getchar())
	" If it is a keyword character, assume starting a valid XML tag, try
	" to autocomplete using the PreTeXt dictionary.
	if nextchar =~ "\\k"
		return "<".nextchar."\<C-x>\<C-k>"
	" If it is ! assume we are writing an XML comment, auto fills.
	elseif nextchar == "!"
		return "<!--    -->\<esc>4hi"
	" If it is / assume we want to close a tag, use Omni-Complete to
	" finish the job. 
	elseif nextchar == "/"
		return "</\<C-x>\<C-o>"
	" If it is another <, assume we want to type the character <, which in
	" PreTeXt should be escaped as \lt. 
	" If followed by anything else assume we want the character < followed
	" by that something else. 
	elseif nextchar == "<"
		return "\\lt "
	else
		return "\\lt".nextchar
	endif
endfunc
"""

" XML does not take <, >, and &. These are used in MathMode lots. 
" PreTeXt escapes them. These are just short hands to make them easier to
" type.
inoremap >> \gt<space> 
inoremap && \amp<space>
inoremap &< &lt;
inoremap &> &gt;
"""

" iabbr maps for PreTeXt Math Mode
iabbr \( <m>
iabbr \[ <me><CR>
iabbr \[n <men><CR>
iabbr \[a <md><CR><mrow>
iabbr #\[a <mdn><CR><mrow>
"""

" mappings for cross referencing in PreTeXt
" ##t is meant for tags to <men> and <mrow> to use symbolic tags instead of
" numbers. 
inoremap ##t <space>tag=""<left><C-R>=ListEqTags()<CR>
" ##n is to add numbering to <md> rows. 
inoremap ##n <space>number="yes"
" ### is to add label
inoremap ### <S-left>xml:id="<S-right>"
" @@ is to cross ref. Can use omni-complete or C-p, C-n to complete tags. 
inoremap @@ <S-left><xref ref="<S-right>" />
" These are the possible symbolic tags to use in PreTeXt
func! ListEqTags()
	call complete(col('.'), ['star', 'dstar', 'tstar', 'dagger', 'ddagger', 'tdagger', 'hash', 'dhash', 'thash', 'maltese', 'dmaltese', 'tmaltese'])
	return ''
endfunc
"""
