# PreTeXt-custom

Just a bunch of my personal customizations for using [PreTeXt](https://pretextbook.org/). 

To generate HTML files use

    xsltproc -xinclude -stringparam publisher ~/MSU-Gitlab/pretext-custom/xsl/www-publisher.xml ~/MSU-Gitlab/pretext-custom/xsl/www-pretext-html.xsl <source>
